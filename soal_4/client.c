#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080
#define SERVER_IP "127.0.0.1"

int main() {
    int sock = 0;
    struct sockaddr_in server_addr;
    char message[1024];

    // Membuat socket klien
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Gagal membuat socket \n");
        return -1;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    // Konversi alamat IPv4 dan mengatur server IP
    if(inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr) <= 0) {
        printf("\nInvalid address/Address not supported \n");
        return -1;
    }

    // Membuat koneksi ke server
    if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        printf("\nGagal menghubungkan ke server \n");
        return -1;
    }

    printf("Koneksi ke server berhasil\n");

    // Kirim pesan ke server
    printf("Masukkan pesan untuk dikirim ke server: ");
    fgets(message, sizeof(message), stdin);
    send(sock, message, strlen(message), 0);
    printf("Pesan terkirim\n");

    return 0;
}
