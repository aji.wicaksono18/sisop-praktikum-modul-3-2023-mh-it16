#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <stdint.h>
#include <unistd.h>

#define WHITESPACE 64
#define EQUALS     65
#define INVALID    66

static const unsigned char d[] = {
    66,66,66,66,66,66,66,66,66,66,64,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,62,66,66,66,63,52,53,
    54,55,56,57,58,59,60,61,66,66,66,65,66,66,66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,66,66,66,66,66,66,26,27,28,
    29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66
};

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen) {
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end) {
        unsigned char c = d[*in++];

        switch (c) {
        case WHITESPACE: continue;   /* skip whitespace */
        case INVALID:    return 1;   /* invalid input, return error */
        case EQUALS:                 /* pad character, end of data */
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; // increment the number of iterations
            /* If the buffer is full, split it into bytes */
            if (iter == 4) {
                if ((len += 3) > *outLen) return 1; /* buffer overflow */
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3) {
        if ((len += 2) > *outLen) return 1; /* buffer overflow */
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2) {
        if (++len > *outLen) return 1; /* buffer overflow */
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; /* modify to reflect the actual output size */
    return 0;
}

// Definition of the msgbuf struct
struct msgbuf {
    long mtype;
    char mtext[1024];
};

#define KEY 1234

// Function to validate credentials
int validateCredentials(const char *username, const char *password) {
    FILE *fp = fopen("users.txt", "r");
    if (fp == NULL) {
        perror("fopen");
        return 0; // Authentication failed
    }

    char line[1024];
    while (fgets(line, sizeof(line), fp)) {
        char *storedUsername = strtok(line, ":");
        char *storedPassword = strtok(NULL, ":");

        // Compare the provided username with stored username
        if (strcmp(username, storedUsername) == 0) {
            // Decrypt the stored password
            size_t msglen = 1024; // Maximum length for decrypted message
            if (base64decode(storedPassword, strlen(storedPassword), (unsigned char *)storedPassword, &msglen) == 0) {
                // Remove padding '=' characters at the end of the string
                while (msglen > 0 && storedPassword[msglen - 1] == '=') {
                    msglen--;
                }

                // If the decrypted password matches the provided password, return success
                if (strcmp(password, (char *)storedPassword) == 0) {
                    fclose(fp);
                    return 1; // Authentication successful
                }
            }
        }
    }

    fclose(fp);
    return 0; // Authentication failed
}


int main() {
    // Create a message queue
    int msgid = (int)(msgget(KEY, IPC_CREAT | 0666));
    if (msgid < 0) {
        perror("msgget");
        exit(1);
    }

    // Prepare a struct to store messages
    struct msgbuf m;

    // Read messages
    while (1) {
        if ((msgrcv(msgid, &m, sizeof(m.mtext), 1, 0)) < 0) {
            perror("msgrcv");
            exit(1);
        }

        // Check the command
        if (strncmp(m.mtext, "CREDS", 5) == 0) {
            // Dekripsi password
            FILE *fp = fopen("users.txt", "r");
            if (fp == NULL) {
                perror("fopen");
                exit(1);
            }

            char line[1024];
            while (fgets(line, sizeof(line), fp)) {
                char *username = strtok(line, ":");
                char *password = strtok(NULL, ":");

                // Dekripsi password
                size_t msglen = 1024; // Maksimum panjang pesan yang dapat didekripsi
                if (base64decode(password, strlen(password), (unsigned char *)password, &msglen) == 0) {
                    // Menghapus karakter padding '=' di akhir string
                    while (msglen > 0 && password[msglen - 1] == '=') {
                        msglen--;
                    }

                    // Tampilkan kredensial
                    printf("Username: %s, Password: %.*s\n", username, (int)msglen, password);
                } else {
                    // Gagal mendekripsi
                    fprintf(stderr, "Gagal mendekripsi password untuk pengguna %s\n", username);
                }
            }
            fclose(fp);
        }

        // Check the command
if (strncmp(m.mtext, "AUTH: ", 6) == 0) {
    // Extract the credentials from the message
    char *credentials = m.mtext + 6;
    char username[128], password[128];

    // Parse the entire line after "AUTH:" as a single string
    if (sscanf(credentials, "%s %[^\n]", username, password) == 2) {
        // Validate the credentials
        if (validateCredentials(username, password)) {
            printf("Authentication successful for user: %s\n", username);
            strcpy(m.mtext, "Authentication successful");
        } else {
            printf("Authentication failed for user: %s\n", username);
            strcpy(m.mtext, "Authentication failed");
        }
    } else {
        printf("Invalid credentials format\n");
        strcpy(m.mtext, "Invalid credentials format");
    }

    // Send the authentication result back
    if (msgsnd(msgid, &m, sizeof(m.mtext), 0) < 0) {
        perror("msgsnd");
        exit(1);
    }
}
    }

    return 0;
}   