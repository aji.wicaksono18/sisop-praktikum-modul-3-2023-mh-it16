#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080
#define MAX_CONNECTIONS 5

int main() {
    int server_fd;
    int new_socket;
    int valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    int client_terhubung = 0;
    int pesan_terkirim = 0;

    // Membuat socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Mengatur opsi socket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Binding address
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Menunggu koneksi, maksimal terhubung 5 koneksi
    if (listen(server_fd, MAX_CONNECTIONS) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d ..\n", PORT);

    // Menerima dan menampilkan pesan
    while (1) {
        if (client_terhubung >= MAX_CONNECTIONS && !pesan_terkirim) {
            printf("Batas koneksi tercapai. Tidak dapat menerima client tambahan.\n");
            pesan_terkirim = 1;
        }
        if (client_terhubung < MAX_CONNECTIONS) {
            pesan_terkirim = 0;
        }
        
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        client_terhubung++; // Menambah jumlah client yg terhubung

        char buffer[1024] = {0};
        valread = read(new_socket, buffer, sizeof(buffer));
        printf("Pesan dari client: %s\n", buffer);
    }

    return 0;
}
