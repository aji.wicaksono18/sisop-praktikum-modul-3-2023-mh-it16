#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

unsigned long long factorial(int n) {
   if (n == 0) return 1;
   return n * factorial(n - 1);
}

struct ThreadData {
    int value;
};

void *calculateFactorial(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    unsigned long long result = factorial(data->value);
    return (void *)result;
}

int main() {
    int ROW1 = 6;
    int COL2 = 6;

    key_t key = 5678;
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666);
    int *shm = (int *)shmat(shmid, NULL, 0);

    int result[ROW1][COL2];

     printf("Matrix Shared Memory:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = shm[i * COL2 + j];
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    int transposed[COL2][ROW1];
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            transposed[j][i] = result[i][j];
        }
    }

    printf("\nMatriks Transpose:\n");
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            printf("%d\t", transposed[i][j]);
        }
        printf("\n");
    }

    printf("\nFaktorial:\n");
    pthread_t threads[ROW1 * COL2];
    struct ThreadData thread_data[ROW1 * COL2];
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            thread_data[i * COL2 + j].value = transposed[i][j];
            pthread_create(&threads[i * COL2 + j], NULL, calculateFactorial, &thread_data[i * COL2 + j]);
        }
    }

    for (int i = 0; i < ROW1 * COL2; i++) {
        unsigned long long result;
        pthread_join(threads[i], (void **)&result);
        printf("%llu\t", result);
        if ((i + 1) % COL2 == 0) {
            printf("\n");
        }
    }

    shmdt(shm);

    return 0;
}
