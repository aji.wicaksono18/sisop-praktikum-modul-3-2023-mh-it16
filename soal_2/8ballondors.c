#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

void membacaFile(char* inputNamaFile, char* outputNamaFile) {
    FILE *inputFile, *outputFile;
    char karakter;

    // Membuka file input
    inputFile = fopen(inputNamaFile, "r");
    if (inputFile == NULL) {
        perror("Gagal membuka file input");
        exit(EXIT_FAILURE);
    }

    // Membuka file output
    outputFile = fopen(outputNamaFile, "w");
    if (outputFile == NULL) {
        perror("Gagal membuka file output");
        exit(EXIT_FAILURE);
    }

    // Proses karakter
    while ((karakter = fgetc(inputFile)) != EOF) {
        if (isalpha(karakter) || karakter == ' ' || karakter == '\n') {
            fputc(karakter, outputFile);
        } else {
            printf("Karakter tidak valid: %c\n", karakter); 
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}

void menghitungKata(char* namaFile, char* wordToCount, int* frekuensi) {
    FILE *file = fopen(namaFile, "r");
    if (file == NULL) {
        perror("Gagal membuka file");
        exit(EXIT_FAILURE);
    }

    char kata[100]; 

    while (fscanf(file, "%s", kata) != EOF) {
        if (strcmp(kata, wordToCount) == 0) { // Dengan case-sensitive
            (*frekuensi)++;
        }
    }

    fclose(file);
}

void menghitungHuruf(char* namaFile, char charToCount, int* frekuensi) {
    FILE *file = fopen(namaFile, "r");
    if (file == NULL) {
        perror("Gagal membuka file");
        exit(EXIT_FAILURE);
    }

    char karakter;

    while ((karakter = fgetc(file)) != EOF) {
        if (karakter == charToCount) {
            (*frekuensi)++;
        }
    }

    fclose(file);
}

void simpanKeLog(char* lognamaFile, char* type, char* message, char* input, int frekuensi) {
    FILE *logFile = fopen(lognamaFile, "a");
    if (logFile == NULL) {
        perror("Gagal membuka file log");
        exit(EXIT_FAILURE);
    }

    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    fprintf(logFile, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n",
            tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
            tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, type, message, input, frekuensi);

    fclose(logFile);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Cara menggunakan: %s -kata/-huruf\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid_t pid;
    int status;
    int pipefd[2];
    int frekuensi = 0;
    char input[100];

    if (pipe(pipefd) == -1) {
        perror("Pipe error");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid < 0) {
        perror("Fork error");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        
        close(pipefd[0]); 

        if (strcmp(argv[1], "-kata") == 0) {
            printf("Masukkan kata yang ingin dihitung jumlah frekuensinya: ");
            if (scanf("%s", input) != 1) {
                printf("Gagal membaca input.\n");
                exit(EXIT_FAILURE);
            }
            menghitungKata("thebeatles.txt", input, &frekuensi);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Masukkan huruf yang ingin dihitung jumlah frekuensinya: ");
            if (scanf(" %c", &input[0]) != 1) {
                printf("Gagal membaca input.\n");
                exit(EXIT_FAILURE);
            }
            menghitungHuruf("thebeatles.txt", input[0], &frekuensi);
        } else {
            printf("Argumen tidak valid.\n");
            exit(EXIT_FAILURE);
        }


        write(pipefd[1], &frekuensi, sizeof(int));
        close(pipefd[1]); 

        exit(EXIT_SUCCESS);
    } else {
        
        close(pipefd[1]); 

        waitpid(pid, &status, 0);
        read(pipefd[0], &frekuensi, sizeof(int));

        printf("File telah diproses dan disimpan sebagai thebeatles.txt\n");

        // Mencetak hasil frekuensi
        if (strcmp(argv[1], "-kata") == 0) {
            printf("Frekuensi kemunculan kata: %d\n", frekuensi);
            simpanKeLog("frekuensi.log", "KATA", "Kata", input, frekuensi);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Frekuensi kemunculan huruf: %d\n", frekuensi);
            simpanKeLog("frekuensi.log", "HURUF", "Huruf", input, frekuensi);
        }

        close(pipefd[0]); 
    }

    return 0;
}
