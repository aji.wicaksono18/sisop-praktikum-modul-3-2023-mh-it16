#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

unsigned long long factorial(int n) {
   if (n == 0) return 1;
   return n * factorial(n - 1);
}

int main() {
    int ROW1 = 6;
    int COL2 = 6;
    
    key_t key = 5678;
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666);
    int *shm = (int *)shmat(shmid, NULL, 0);

    int result[ROW1][COL2];

    printf("Matrix Shared Memory:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = shm[i * COL2 + j];
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    int transposed[COL2][ROW1];
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            transposed[j][i] = result[i][j];
        }
    }

    printf("\nMatriks Transpose:\n");
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            printf("%d\t", transposed[i][j]);
        }
        printf("\n");
    }

    printf("\nFaktorial:\n");
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            printf("%llu\t", factorial(transposed[i][j]));
        }
        printf("\n");
    }

    shmdt(shm);

    return 0;
}
