#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

int main() {
    srand(time(NULL));

    int ROW1 = 6; //kelompok 16
    int COL1 = 2;
    int ROW2 = 2;
    int COL2 = 6; //kelompok 16

    key_t key = 5678;
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, IPC_CREAT | 0666);
    int *shm;
    shm = shmat(shmid, NULL, 0);

    int sum = 0;
    int Mat1[ROW1][COL1]; 
    int Mat2[ROW2][COL2];
    int result[ROW1][COL2];

    printf("Matriks 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            Mat1[i][j] = rand() % 4 + 1;
            printf("%d\t", Mat1[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            Mat2[i][j] = rand() % 5 + 1;
            printf("%d\t", Mat2[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            for (int k = 0; k < COL1; k++) {
                sum = sum + Mat1[i][k] * Mat2[k][j];
            }
            result[i][j] = sum;
            sum = 0;
        }
    }

    printf("\nHasil Kali Matriks:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    printf("\nHasil Matriks Setelah Dikurang 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] -= 1;
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shm[i * COL2 + j] = (result[i][j]);
        }
    }
    
    shmdt(shm);

    return 0;
}