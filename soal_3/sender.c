#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

// Definition of the msgbuf struct
struct msgbuf {
  long mtype;
  char mtext[1024];
};

#define KEY 1234

int main() {
  // Buat message queue
  int msgid = msgget(KEY, IPC_CREAT | 0666);
  if (msgid < 0) {
    perror("msgget");
    exit(1);
  }

  // Kirim perintah "CREDS"
  char *pesan = "CREDS";
  struct msgbuf m;
  m.mtype = 1;
  strcpy(m.mtext, pesan);
  if (msgsnd(msgid, &m, sizeof(pesan), 0) < 0) {
    perror("msgsnd");
    exit(1);
  }

  // Tunggu balasan untuk "CREDS"
  struct msgbuf balasan;
  if (msgrcv(msgid, &balasan, sizeof(balasan.mtext), 1, 0) < 0) {
    perror("msgrcv");
    exit(1);
  }

  // Tampilkan balasan untuk "CREDS"
  printf("Balasan (CREDS): %s\n", balasan.mtext);

  // Hardcode the username and password for "AUTH"
  char username[128] = "username";
  char password[128] = "password";

  // Create the "AUTH: username password" message
  char pesan_auth[256];
  snprintf(pesan_auth, sizeof(pesan_auth), "AUTH: %s %s", username, password);

  // Send the authentication message
  struct msgbuf m_auth;
  m_auth.mtype = 1;
  strcpy(m_auth.mtext, pesan_auth);
  if (msgsnd(msgid, &m_auth, sizeof(pesan_auth), 0) < 0) {
    perror("msgsnd");
    exit(1);
  }

  // Tunggu balasan untuk "AUTH"
  struct msgbuf balasan_auth;
  if (msgrcv(msgid, &balasan_auth, sizeof(balasan_auth.mtext), 1, 0) < 0) {
    perror("msgrcv");
    exit(1);
  }

  // Tampilkan balasan untuk "AUTH"
  printf("Balasan (AUTH): %s\n", balasan_auth.mtext);

  // Hapus message queue
  if (msgctl(msgid, IPC_RMID, NULL) < 0) {
    perror("msgctl");
    exit(1);
  }

  return 0;
}
