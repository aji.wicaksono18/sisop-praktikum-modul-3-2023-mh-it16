**Anggota Kelompok:**

| Nama | NRP |
| ------ | ------ |
|Rahmad Aji Wicaksono       |5027221034        |
|Alma Amira Dewani          |5027221054        |
|Zidny Ilman Na'fian        |5027221072        |

# Soal 1
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :


## Cara Pengerjaan

### Point A
Membuat program C dengan nama **belajar.c**, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [nomor_kelompok]×2 dan matriks kedua 2×[nomor_kelompok]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks  kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. 

#### Code
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>

int main() {
    // Inisialisasi seed generator angka acak
    srand(time(NULL));

    // Deklarasi ukuran matriks
    int ROW1 = 6; //kelompok 16
    int COL1 = 2;
    int ROW2 = 2;
    int COL2 = 6; //kelompok 16

    // Inisialisasi variabel
    int sum = 0;
    int Mat1[ROW1][COL1]; 
    int Mat2[ROW2][COL2];
    int result[ROW1][COL2];

    // Mengisi Matriks 1 dengan angka acak
    printf("Matriks 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL1; j++) {
            Mat1[i][j] = rand() % 4 + 1;
            printf("%d\t", Mat1[i][j]);
        }
        printf("\n");
    }

    // Mengisi Matriks 2 dengan angka acak
    printf("\nMatriks 2:\n");
    for (int i = 0; i < ROW2; i++) {
        for (int j = 0; j < COL2; j++) {
            Mat2[i][j] = rand() % 5 + 1;
            printf("%d\t", Mat2[i][j]);
        }
        printf("\n");
    }

    // Perkalian matriks
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            for (int k = 0; k < COL1; k++) {
                sum = sum + Mat1[i][k] * Mat2[k][j];
            }
            result[i][j] = sum;
            sum = 0;
        }
    }

    // Menampilkan hasil perkalian matriks
    printf("\nHasil Kali Matriks:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    return 0;
}

```

#### Penjelasan Code

- `srand(time(NULL));` : Menginisialisasi generator angka acak dengan menggunakan waktu saat ini sebagai seed.
Deklarasi Ukuran Matriks:
- `int ROW1 = 6; //kelompok 16`: Mendeklarasikan jumlah baris matriks pertama.
- `int COL1 = 2;`: Mendeklarasikan jumlah kolom matriks pertama.
- `int ROW2 = 2;`: Mendeklarasikan jumlah baris matriks kedua.
- `int COL2 = 6; //kelompok 16`: Mendeklarasikan jumlah kolom matriks kedua.
- `int sum = 0;`: Membuat variabel untuk menyimpan penjumlahan sementara.
- `int Mat1[ROW1][COL1];`: Matriks pertama dengan ukuran yang sudah ditentukan.
- `int Mat2[ROW2][COL2];`: Matriks kedua dengan ukuran yang sudah ditentukan.
- `int result[ROW1][COL2];`: Matriks hasil perkalian dengan ukuran yang sesuai.
- Loop pertama digunakan untuk mengisi setiap elemen matriks 1 dengan angka acak antara 1 hingga 4 menggunakan fungsi `rand().`
- Setiap elemen yang dihasilkan ditampilkan di baris yang sama.
- Loop kedua serupa dengan loop pertama, namun kali ini digunakan untuk mengisi setiap elemen matriks 2 dengan angka acak antara 1 hingga 5 menggunakan fungsi `rand().`
- Setiap elemen yang dihasilkan ditampilkan di baris yang sama setelah setiap iterasi.
- Terdapat tiga loop bersarang yang melakukan operasi perkalian matriks antara matriks 1 dan matriks 2.
- Setiap elemen dari matriks hasil dihitung berdasarkan - aturan perkalian matriks yang sesuai.
- Pada setiap iterasi, nilai dari matriks hasil diinisialisasi dengan 0 (sum = 0).
- Setiap elemen matriks hasil dihitung dengan menjumlahkan hasil perkalian dari setiap elemen matriks pertama dan matriks kedua sesuai aturan perkalian matriks.
- Setelah selesai mengalikan elemen-elemen matriks pertama dan matriks kedua untuk elemen tertentu dari matriks hasil, nilai sum direset kembali menjadi 0.
- Hasil dari perkalian setiap pasangan elemen matriks pertama dan matriks kedua disimpan dalam matriks hasil (result) pada posisi yang sesuai dengan indeks baris dan kolom.
- Loop terakhir digunakan untuk mencetak hasil dari perkalian matriks ke layar.
- Setiap elemen dari matriks hasil dicetak di baris yang sama setelah setiap iterasi.
Kembali dari Fungsi Main:
- Fungsi `main` kembali mengembalikan nilai 0, menunjukkan bahwa program selesai dijalankan tanpa adanya kesalahan.


### Point B
Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.

#### Code 

```c
 // Mengurangi setiap elemen matriks hasil dengan 1
    printf("\nHasil Matriks Setelah Dikurang 1:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] -= 1;
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }
```

#### Penjelasan Code
- Loop pertama dan kedua digunakan untuk mengakses setiap elemen dari matriks hasil.
- Setiap elemen dari matriks hasil dikurangi dengan angka 1 menggunakan operator pengurangan (-=).
- Mencetak Hasil Matriks yang Dikurangi 1:
- Setelah setiap elemen dari matriks hasil dikurangi dengan 1, program mencetak matriks yang telah dimodifikasi ke layar.
- Setiap elemen dari matriks hasil yang telah dikurangi 1 dicetak di baris yang sama setelah setiap iterasi.


### Point C
Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama **yang.c.** Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya.

**(Catatan: wajib menerapkan konsep shared memory)**

#### Code

**Menerapkan konsep Shared Memory ke belajar.c**

```c
// Menginisialisasi shared memory key
    key_t key = 5678;
    
    // Mendapatkan akses ke shared memory
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, IPC_CREAT | 0666);
    int *shm;
    shm = shmat(shmid, NULL, 0);
```

**Penjelasan Kode:**
- Sebuah kunci (key) shared memory diinisialisasi dengan nilai 5678. Kunci ini digunakan untuk mengidentifikasi dan mengakses area shared memory yang sesuai.
- Fungsi `shmget` digunakan untuk membuat atau mendapatkan shared memory segment. Di sini, ukuran shared memory ditentukan dengan mengalikan ukuran elemen matriks (dalam byte) dengan jumlah total elemen matriks yang diinginkan, yaitu `sizeof(int) * ROW1 * COL2. Parameter IPC_CREAT | 0666` memberikan hak akses yang sesuai terhadap shared memory yang baru dibuat.
- Fungsi `shmat` (shared memory attach) menempelkan (attach) shared memory segment yang telah diperoleh ke ruang alamat proses saat ini. Nilai parameter kedua `(NULL)` menunjukkan bahwa sistem operasi harus memilih alamat yang tepat untuk meletakkan segment tersebut. Nilai 0 pada parameter ketiga menentukan bahwa segment tersebut tidak di-attach ke alamat tertentu, melainkan di-attach ke alamat yang tersedia secara otomatis.



```c
 // Menyimpan hasil matriks ke dalam shared memory
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            shm[i * COL2 + j] = (result[i][j]);
        }
    }
    
    // Melepaskan akses ke shared memory
    shmdt(shm);
```

**Penjelasan Kode:**
- Dua loop bersarang digunakan untuk mengakses setiap elemen dalam matriks hasil.
- Setiap elemen dari matriks hasil disalin ke area shared memory menggunakan indeks yang sesuai. Ekspresi `shm[i * COL2 + j]` digunakan untuk menetapkan nilai matriks hasil ke area shared memory yang sesuai.
- Fungsi `shmdt` (shared memory detach) digunakan untuk melepaskan atau mendetach shared memory segment yang telah dilampirkan sebelumnya.
- Parameter `shm` mengacu pada alamat dari shared memory segment yang akan dilepaskan.


**Membuat file yang.c lalu mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c dan melakukan transpose matrix**

```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int main() {
    // Inisialisasi ukuran matriks
    int ROW1 = 6;
    int COL2 = 6;

    // Inisialisasi shared memory key dan akses shared memory
    key_t key = 5678;
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666);
    int *shm = (int *)shmat(shmid, NULL, 0);

    // Matriks untuk menyimpan data dari shared memory
    int result[ROW1][COL2];

    // Mencetak matriks dari shared memory
    printf("Matrix Shared Memory:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = shm[i * COL2 + j];
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    // Transpose matriks
    int transposed[COL2][ROW1];
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            transposed[j][i] = result[i][j];
        }
    }

    // Mencetak matriks transpose
    printf("\nMatriks Transpose:\n");
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            printf("%d\t", transposed[i][j]);
        }
        printf("\n");
    }

    // Melepaskan akses shared memory
    shmdt(shm);

    return 0;
}

```
#### Penjelasan Kode

- Dua variabel ROW1 dan COL2 diinisialisasi dengan nilai 6, menunjukkan jumlah baris dan kolom untuk matriks yang akan dioperasikan.
- Sebuah kunci (key) shared memory diinisialisasi dengan nilai 5678.
- Fungsi `shmget` digunakan untuk membuat atau mendapatkan segmen shared memory.
- Ukuran dari segmen shared memory dihitung dengan mengalikan ukuran elemen matriks (dalam byte) dengan jumlah total elemen matriks yang diinginkan, yaitu `sizeof(int) * ROW1 * COL2`.
- Hak akses `0666` diberikan untuk memberikan hak baca dan tulis penuh ke segmen shared memory yang dibuat.
- Matriks `result` dideklarasikan dengan ukuran yang telah ditentukan sebelumnya, yaitu `ROW1` baris dan `COL2` kolom. Matriks ini akan digunakan untuk menyimpan data yang diperoleh dari shared memory setelah pengambilan.
- Sebuah pernyataan `printf` digunakan untuk mencetak pesan "Matrix Shared Memory:" ke layar.
- Dua loop bersarang digunakan untuk mengisi matriks result dengan nilai yang diperoleh dari area shared memory.
- Ekspresi `shm[i * COL2 + j]` digunakan untuk mengambil nilai yang sesuai dari shared memory dan menyimpannya di matriks result.
- Setiap elemen matriks yang telah disalin ke `result` dicetak di layar dengan menggunakan pernyataan `printf` bersama dengan karakter tab `\t.`
- Sebuah matriks `transposed` diinisialisasi dengan ukuran `COL2` baris dan `ROW1` kolom. Ukuran ini dipilih untuk menampung matriks hasil transposisi.
- Dua loop bersarang digunakan untuk memindahkan elemen-elemen dari matriks `result` ke matriks `transposed` dengan melakukan transposisi.
- Setiap elemen `result[i][j]` dipindahkan ke `transposed[j][i]` untuk memastikan bahwa elemen-elemen matriks berubah posisi sesuai aturan transposisi.
- Sebuah pernyataan `printf` digunakan untuk mencetak pesan "Matriks Transpose:" ke layar.
- Dua loop bersarang digunakan untuk mencetak setiap elemen dari matriks `transposed ke` layar.
- Setiap elemen dari matriks `transposed` dicetak di layar menggunakan pernyataan `printf`, dan setelah setiap elemen, karakter tab `(\t)` digunakan untuk memformat output.
- Fungsi `shmdt` digunakan untuk melepaskan (detach) segment shared memory yang telah dilekatkan sebelumnya.
- Parameter `shm` digunakan untuk mengacu pada alamat dari segment shared memory yang akan dilepaskan.
- Pernyataan `return 0` digunakan untuk mengakhiri program dengan mengembalikan nilai 0, menunjukkan bahwa program telah selesai dijalankan dengan sukses.


### Point D
Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

**(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)**

#### Code
```c
// Fungsi rekursif untuk menghitung faktorial
unsigned long long factorial(int n) {
    if (n == 0) return 1;
    return n * factorial(n - 1);
}

// Struktur data untuk menyimpan nilai thread
struct ThreadData {
    int value;
};

// Fungsi thread untuk menghitung faktorial
void *calculateFactorial(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    // Menghitung faktorial dari nilai yang diberikan
    unsigned long long result = factorial(data->value);
    // Mengembalikan hasil faktorial sebagai pointer void
    return (void *)result;
}
```
```c
 // Menghitung faktorial untuk setiap elemen matriks transpose menggunakan thread
    printf("\nFaktorial:\n");
    pthread_t threads[ROW1 * COL2];
    struct ThreadData thread_data[ROW1 * COL2];
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            thread_data[i * COL2 + j].value = transposed[i][j];
            pthread_create(&threads[i * COL2 + j], NULL, calculateFactorial, &thread_data[i * COL2 + j]);
        }
    }

    // Menunggu semua thread selesai dan mencetak hasil faktorial
    for (int i = 0; i < ROW1 * COL2; i++) {
        unsigned long long result;
        pthread_join(threads[i], (void **)&result);
        printf("%llu\t", result);
        if ((i + 1) % COL2 == 0) {
            printf("\n");
        }
    }
```

#### Penjelasan Code
- Fungsi `factorial` digunakan untuk menghitung nilai faktorial dari bilangan yang diberikan secara rekursif. Jika nilai n adalah 0, maka fungsi ini akan mengembalikan 1. Jika tidak, nilai faktorial dihitung dengan mengalikan n dengan faktorial dari n - 1.
- Sebuah struktur data `ThreadData` digunakan untuk menyimpan nilai yang akan diproses oleh setiap thread.
- Fungsi `calculateFactorial` merupakan fungsi thread yang bertanggung jawab untuk menghitung faktorial dari nilai yang diberikan.  
- Struktur data `ThreadData` digunakan untuk mengambil nilai yang akan dihitung faktorialnya.
- Hasil faktorial dari nilai tersebut dihitung menggunakan fungsi rekursif `factorial` dan kemudian dikembalikan sebagai pointer void.
- Dua loop bersarang digunakan untuk melakukan iterasi melalui setiap elemen dari matriks `transposed`.
- Setiap elemen matriks `transposed` diberikan ke thread yang sesuai untuk menghitung faktorialnya menggunakan fungsi `pthread_create`.
- Setelah menunggu semua thread selesai dengan menggunakan `pthread_join`, hasil faktorial dari setiap elemen dicetak di layar.
- Sebuah pernyataan `printf` juga digunakan untuk memformat output sehingga hasil faktorial dicetak secara berbaris, tergantung pada nilai dari `COL2`.


### Point E
Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama **rajin.c.** Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 

#### Code
```c
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi rekursif untuk menghitung faktorial
unsigned long long factorial(int n) {
    if (n == 0) return 1;
    return n * factorial(n - 1);
}

int main() {
    // Inisialisasi ukuran matriks
    int ROW1 = 6;
    int COL2 = 6;
    
    // Inisialisasi shared memory key dan akses shared memory
    key_t key = 5678;
    int shmid = shmget(key, sizeof(int) * ROW1 * COL2, 0666);
    int *shm = (int *)shmat(shmid, NULL, 0);

    // Matriks untuk menyimpan data dari shared memory
    int result[ROW1][COL2];

    // Mencetak matriks dari shared memory
    printf("Matrix Shared Memory:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            result[i][j] = shm[i * COL2 + j];
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    // Transpose matriks
    int transposed[COL2][ROW1];
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            transposed[j][i] = result[i][j];
        }
    }

    // Mencetak matriks transpose dan nilai faktorial dari setiap elemen
    printf("\nMatriks Transpose:\n");
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            printf("%d\t", transposed[i][j]);
        }
        printf("\n");
    }

    // Menghitung faktorial untuk setiap elemen matriks transpose
    printf("\nFaktorial:\n");
    for (int i = 0; i < COL2; i++) {
        for (int j = 0; j < ROW1; j++) {
            printf("%llu\t", factorial(transposed[i][j]));
        }
        printf("\n");
    }

    // Melepaskan akses shared memory
    shmdt(shm);

    return 0;
}

```

#### Penjelasan Code
- Fungsi `factorial` menghitung faktorial dari bilangan yang diberikan menggunakan metode rekursif. Jika nilai yang diberikan adalah 0, maka fungsi akan mengembalikan 1.
- Ukuran matriks diinisialisasi dengan ROW1 dan COL2.
- Dibuat shared memory dengan kunci yang telah ditentukan sebelumnya.
- Dalam hal ini, `shmget` dan `shmat` digunakan untuk mendapatkan akses ke shared memory.
- Nilai matriks yang disimpan dalam shared memory dicetak ke layar dengan format yang sesuai.
- Matriks hasil dari shared memory ditransposisikan menggunakan loop berulang. Hasil transposisi disimpan dalam matriks `transposed`.
- Matriks hasil transposisi dicetak ke layar.
- Setiap elemen dari matriks transposisi dihitung faktorialnya dan dicetak ke layar.
- Akses ke shared memory dilepaskan menggunakan `shmdt`.
- Fungsi `main` mengembalikan nilai 0, menandakan bahwa program telah berhasil dieksekusi.


## Output

### Output belajar.c
![output 1 ](gambar/1output-belajar.png)

### Output yang.c
![output 2 ](gambar/1output-yang.png)

### Output rajin.c
![output 3 ](gambar/1output-rajin.png)

### Perbandingan program yang.c dan rajin.C
menjalankan command
```c
time ./yang
time ./rajin
```

**yang.c**

![output 4 ](gambar/1time-yang.png)

**rajin.c**

![output 5 ](gambar/1time-rajin.png)

## Kendala 
- Kesulitan saat menampilkan hasil dari matrix shared memory karena hasil pengurangan matrix tidak tersimpan di shared memory.


# Soal 2
## Deskripsi Program
Program ini memiliki dua bagian utama yang berjalan dalam dua proses terpisah (parent process dan child process). Berikut adalah deskripsi dari masing-masing bagian:

# Langkah-langkah Pengerjaan
### Bagian A
Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file. Hasil dari proses ini akan disimpan dalam file dengan nama `thebeatles.txt`.

#### 1. Membuat fungsi untuk membaca file input dan output.
Kode :
```c
void membacaFile(char* inputNamaFile, char* outputNamaFile) {
    FILE *inputFile, *outputFile;
    char karakter;

    // Membuka file input
    inputFile = fopen(inputNamaFile, "r");
    if (inputFile == NULL) {
        perror("Gagal membuka file input");
        exit(EXIT_FAILURE);
    }

    // Membuka file output
    outputFile = fopen(outputNamaFile, "w");
    if (outputFile == NULL) {
        perror("Gagal membuka file output");
        exit(EXIT_FAILURE);
    }

    // Proses karakter
    while ((karakter = fgetc(inputFile)) != EOF) {
        if (isalpha(karakter) || karakter == ' ' || karakter == '\n') {
            fputc(karakter, outputFile);
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}
```
Penjelasan Kode :

Pada poin ini, terdapat fungsi `membacaFile` yang bertanggung jawab membaca file input, membersihkan karakter-karakter yang bukan huruf, dan membuat file output baru dengan nama yang diinginkan. Fungsi ini membutuhkan dua argumen yaitu `inputNamaFile` (nama file input) dan `outputNamaFile` (nama file output).

### Bagian B
Pada child process, program akan melakukan perhitungan jumlah frekuensi kemunculan sebuah kata dari file `thebeatles.txt`. Frekuensi kata yang dihitung akan dikirim kembali ke parent process.

#### 2. Membuat fungsi untuk melakukan perhitungan jumlah frekuensi kemunculan sebuah kata
Kode :
```c
void menghitungKata(char* namaFile, char* wordToCount, int* frekuensi) {
    FILE *file = fopen(namaFile, "r");
    if (file == NULL) {
        perror("Gagal membuka file");
        exit(EXIT_FAILURE);
    }

    char kata[100]; 

    while (fscanf(file, "%s", kata) != EOF) {
        if (strcmp(kata, wordToCount) == 0) { // Dengan case-sensitive
            (*frekuensi)++;
        }
    }

    fclose(file);
}
```
Penjelasan Kode :
Fungsi `menghitungKata` digunakan untuk menghitung jumlah kemunculan kata tertentu dalam file output. Fungsi ini membutuhkan tiga argumen yaitu `namaFile` (nama file yang akan diperiksa), `wordToCount` (kata yang akan dihitung frekuensinya), dan `frekuensi` (variabel untuk menyimpan hasil perhitungan).

### Bagian C
Pada child process, program akan melakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari file `thebeatles.txt`. Frekuensi huruf yang dihitung akan dikirim kembali ke parent process.

#### 3. Membuat fungsi untuk melakukan perhitungan jumlah frekuensi kemunculan sebuah huruf
Kode :
```c
void menghitungHuruf(char* namaFile, char charToCount, int* frekuensi) {
    FILE *file = fopen(namaFile, "r");
    if (file == NULL) {
        perror("Gagal membuka file");
        exit(EXIT_FAILURE);
    }

    char karakter;

    while ((karakter = fgetc(file)) != EOF) {
        if (karakter == charToCount) {
            (*frekuensi)++;
        }
    }

    fclose(file);
}
```
Penjelasan Kode :
Fungsi `menghitungHuruf` digunakan untuk menghitung jumlah kemunculan huruf tertentu dalam file output. Fungsi ini membutuhkan tiga argumen yaitu `namaFile` (nama file yang akan diperiksa), `charToCount` (huruf yang akan dihitung frekuensinya), dan `frekuensi` (variabel untuk menyimpan hasil perhitungan).

### Menghubungkan Program dengan Argumen, menggunakan konsep pipes dan fork
```c
int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Cara menggunakan: %s -kata/-huruf\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid_t pid;
    int status;
    int pipefd[2];
    int frekuensi = 0;
    char input[100];

    if (pipe(pipefd) == -1) {
        perror("Pipe error");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid < 0) {
        perror("Fork error");
        exit(EXIT_FAILURE);
    }
```
Penjelasan Kode :
Fungsi `main` merupakan inti dari program. Di awal fungsi ini, kita periksa apakah argumen yang diberikan sesuai dengan yang diharapkan. Program ini membutuhkan satu argumen, yaitu `-kata` atau `-huruf`. Jika argumen tidak sesuai, program akan memberikan pesan cara penggunaan dan keluar.

### D.  Program ini dapat dijalankan dengan dua argumen, lalu memanggil Fungsi membaca file, menghitung kata, menghitung huruf.
1. Untuk menghitung kata: ./8ballondors -kata
2. Untuk menghitung huruf: ./8ballondors -huruf

Kode :
```c
if (strcmp(argv[1], "-kata") == 0) {
            printf("Masukkan kata yang ingin dihitung jumlah frekuensinya: ");
            if (scanf("%s", input) != 1) {
                printf("Gagal membaca input.\n");
                exit(EXIT_FAILURE);
            }
            membacaFile("lirik.txt", "thebeatles.txt"); // Memanggil fungsi membaca
            menghitungKata("thebeatles.txt", input, &frekuensi);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Masukkan huruf yang ingin dihitung jumlah frekuensinya: ");
            if (scanf(" %c", &input[0]) != 1) {
                printf("Gagal membaca input.\n");
                exit(EXIT_FAILURE);
            }
            membacaFile("lirik.txt", "thebeatles.txt");
            menghitungHuruf("thebeatles.txt", input[0], &frekuensi);
        } else {
            printf("Argumen tidak valid.\n");
            exit(EXIT_FAILURE);
        }
```
Penjelasan Kode :
1. Di dalam kondisi ini, program memeriksa apakah argumen yang diberikan adalah `-kata`. Jika ya, maka program akan meminta pengguna memasukkan kata yang ingin dihitung frekuensinya. Kemudian, program akan membaca file input, membersihkan karakter non-huruf, dan menghitung frekuensi kemunculan kata tersebut. Hasilnya akan disimpan di variabel `frekuensi`.
2. Di dalam kondisi ini, program memeriksa apakah argumen yang diberikan adalah `-huruf`. Jika ya, maka program akan meminta pengguna memasukkan huruf yang ingin dihitung frekuensinya. Kemudian, program akan membaca file input, membersihkan karakter non-huruf, dan menghitung frekuensi kemunculan huruf tersebut. Hasilnya akan disimpan di variabel `frekuensi`.

### Bagian E
Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.

```c
    } else {
        
        close(pipefd[1]); 

        waitpid(pid, &status, 0);
        read(pipefd[0], &frekuensi, sizeof(int));

        printf("File telah diproses dan disimpan sebagai thebeatles.txt\n");

        // Mencetak hasil frekuensi
        if (strcmp(argv[1], "-kata") == 0) {
            printf("Frekuensi kemunculan kata: %d\n", frekuensi);
            simpanKeLog("frekuensi.log", "KATA", "Kata", input, frekuensi);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Frekuensi kemunculan huruf: %d\n", frekuensi);
            simpanKeLog("frekuensi.log", "HURUF", "Huruf", input, frekuensi);
        }

        close(pipefd[0]); 
    }
```
Penjelasan Kode:

Di dalam kondisi ini, program akan membaca hasil perhitungan frekuensi dari pipe yang terhubung dengan child process. Hasilnya akan disimpan di variabel frekuensi.

### Bagian F
Membuat fungsi menyimpan ke file frekunsi.log

```c
void simpanKeLog(char* lognamaFile, char* type, char* message, char* input, int frekuensi) {
    FILE *logFile = fopen(lognamaFile, "a");
    if (logFile == NULL) {
        perror("Gagal membuka file log");
        exit(EXIT_FAILURE);
    }

    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    fprintf(logFile, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s \"%s\" muncul sebanyak %d kali dalam file 'thebeatles.txt'\n",
            tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
            tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, type, message, input, frekuensi);

    fclose(logFile);
}
```
Penjelasan Kode :
Fungsi `simpanKeLog` bertanggung jawab untuk menyimpan hasil perhitungan frekuensi ke dalam file log dengan format yang telah ditentukan. Fungsi ini membutuhkan lima argumen yaitu `lognamaFile` (nama file log), `type` (tipe data yang dihitung, apakah kata atau huruf), `message` (pesan untuk log), `input` (kata atau huruf yang dihitung), dan `frekuensi` (hasil frekuensi).

### **Script full :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

void membacaFile(char* inputNamaFile, char* outputNamaFile) {
    FILE *inputFile, *outputFile;
    char karakter;

    // Membuka file input
    inputFile = fopen(inputNamaFile, "r");
    if (inputFile == NULL) {
        perror("Gagal membuka file input");
        exit(EXIT_FAILURE);
    }

    // Membuka file output
    outputFile = fopen(outputNamaFile, "w");
    if (outputFile == NULL) {
        perror("Gagal membuka file output");
        exit(EXIT_FAILURE);
    }

    // Proses karakter
    while ((karakter = fgetc(inputFile)) != EOF) {
        if (isalpha(karakter) || karakter == ' ' || karakter == '\n') {
            fputc(karakter, outputFile);
        }
    }

    fclose(inputFile);
    fclose(outputFile);
}

void menghitungKata(char* namaFile, char* wordToCount, int* frekuensi) {
    FILE *file = fopen(namaFile, "r");
    if (file == NULL) {
        perror("Gagal membuka file");
        exit(EXIT_FAILURE);
    }

    char kata[100]; 

    while (fscanf(file, "%s", kata) != EOF) {
        if (strcmp(kata, wordToCount) == 0) { // Dengan case-sensitive
            (*frekuensi)++;
        }
    }

    fclose(file);
}

void menghitungHuruf(char* namaFile, char charToCount, int* frekuensi) {
    FILE *file = fopen(namaFile, "r");
    if (file == NULL) {
        perror("Gagal membuka file");
        exit(EXIT_FAILURE);
    }

    char karakter;

    while ((karakter = fgetc(file)) != EOF) {
        if (karakter == charToCount) {
            (*frekuensi)++;
        }
    }

    fclose(file);
}

void simpanKeLog(char* lognamaFile, char* type, char* message, char* input, int frekuensi) {
    FILE *logFile = fopen(lognamaFile, "a");
    if (logFile == NULL) {
        perror("Gagal membuka file log");
        exit(EXIT_FAILURE);
    }

    time_t t;
    struct tm *tm_info;

    time(&t);
    tm_info = localtime(&t);

    fprintf(logFile, "[%02d/%02d/%02d %02d:%02d:%02d] [%s] %s \"%s\" muncul sebanyak %d kali dalam file 'thebeatles.txt'\n",
            tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
            tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, type, message, input, frekuensi);

    fclose(logFile);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Cara menggunakan: %s -kata/-huruf\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid_t pid;
    int status;
    int pipefd[2];
    int frekuensi = 0;
    char input[100];

    if (pipe(pipefd) == -1) {
        perror("Pipe error");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if (pid < 0) {
        perror("Fork error");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        
        close(pipefd[0]); 

        if (strcmp(argv[1], "-kata") == 0) {
            printf("Masukkan kata yang ingin dihitung jumlah frekuensinya: ");
            if (scanf("%s", input) != 1) {
                printf("Gagal membaca input.\n");
                exit(EXIT_FAILURE);
            }
            membacaFile("lirik.txt", "thebeatles.txt");
            menghitungKata("thebeatles.txt", input, &frekuensi);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Masukkan huruf yang ingin dihitung jumlah frekuensinya: ");
            if (scanf(" %c", &input[0]) != 1) {
                printf("Gagal membaca input.\n");
                exit(EXIT_FAILURE);
            }
            membacaFile("lirik.txt", "thebeatles.txt");
            menghitungHuruf("thebeatles.txt", input[0], &frekuensi);
        } else {
            printf("Argumen tidak valid.\n");
            exit(EXIT_FAILURE);
        }

        write(pipefd[1], &frekuensi, sizeof(int));
        close(pipefd[1]); 

        exit(EXIT_SUCCESS);
    } else {
        
        close(pipefd[1]); 

        waitpid(pid, &status, 0);
        read(pipefd[0], &frekuensi, sizeof(int));

        printf("File telah diproses dan disimpan sebagai thebeatles.txt\n");

        // Mencetak hasil frekuensi
        if (strcmp(argv[1], "-kata") == 0) {
            printf("Frekuensi kemunculan kata: %d\n", frekuensi);
            simpanKeLog("frekuensi.log", "KATA", "Kata", input, frekuensi);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            printf("Frekuensi kemunculan huruf: %d\n", frekuensi);
            simpanKeLog("frekuensi.log", "HURUF", "Huruf", input, frekuensi);
        }

        close(pipefd[0]); 
    }

    return 0;
}
```

### Cara menjalankan program
**Menggunakan terminal**

1. Download file **lirik.txt**, kemudian cek isinya.

![cek lirik](gambar/cek-lirik.png)

2. Kompilasi program menggunakan perintah berikut di terminal 
```bash
gcc 8ballondors.c -o 8ballondors
```
lalu periksa apakah sudah terbuat file kompilasinya.

![kompilasi](gambar/kompilasi-file.png)

3. Menjalankan program di terminal dengan command `$ ./8ballondors -kata` atau `$ ./8ballondors -huruf`, lalu masukkan masukkan kata atau huruf yang mau dihitung frekuensinya.

Ini menjalankan argumen kata
```bash
./8ballondors -kata
```

![run kata](gambar/run-kata.png)

Ini menjalankan argumen huruf 
```bash
./8ballondors -huruf
```

![run huruf](gambar/run-huruf.png)

Ketika sudah di run, maka akan terbuat otomatis sebuah file **thebeatles.txt** dan **frekuensi.log**

![cek file](gambar/cek-file.png)

4. Cek file **thebeatles.txt** menggunakan perintah berikut di terminal untuk melihat apakah sudah berhasil diproses melakukan penghapusan karakter yang diinginkan.
```bash
nano thebeatles.txt
```

![cek thebeatles](gambar/cek-thebeatles.png)

5. Lalu cek file **frekuensi.log** menggunakan perintah berikut di terminal apakah sudah mencatat sebuah log.
```bash
nano frekuensi.log
```

![cek log](gambar/cek-log.png)


# Soal 3
Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut :
## Cara Pengerjaan

**A.** Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.

Kode sender.c :
```c
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
} message;

int main()
{
    key_t key;
    int msgid;

    key = ftok(".", 18);

    msgid = msgget(key, 0666 | IPC_CREAT);

    message.mesg_type = 1;

    while (1)
    {
        printf("Input perintah: \n");
        fgets(message.mesg_text, 100, stdin);

        msgsnd(msgid, &message, sizeof(message), 0);
    }
    return 0;
}
```
Penjelasan :
<stdio.h>: Header untuk fungsi input dan output standar.
<sys/ipc.h>: Header untuk definisi yang diperlukan untuk bekerja dengan kunci (key) IPC.
<sys/msg.h>: Header  definisi yang diperlukan untuk bekerja dengan antrean pesan (message queue).
<string.h>: Header ini digunakan untuk berbagai operasi yang melibatkan manipulasi string.
<unistd.h>: Header ini berisi berbagai definisi yang digunakan dalam sistem Unix, termasuk beberapa konstanta seperti IPC_CREAT.
mesg_type: Digunakan untuk menentukan jenis pesan. Dalam kode diatas, pesan memiliki jenis 1.
mesg_text: Digunakan untuk menyimpan teks pesan dengan panjang maksimum 100 karakter.
key_t key: Digunakan untuk menyimpan kunci (key) yang akan digunakan untuk mengidentifikasi antrean pesan. Kunci ini dihasilkan dengan menggunakan fungsi ftok(), yang berdasarkan pada lokasi file dan angka integer (18).
msgid = msgget(key, 0666 | IPC_CREAT): Fungsi msgget() digunakan untuk membuat atau mengakses antrean pesan dengan menggunakan kunci key. 0666 adalah izin (permissions) yang diberikan kepada antrean pesan, dan IPC_CREAT menunjukkan bahwa antrean pesan akan dibuat jika belum ada.
message.mesg_type = 1: Program mengatur jenis pesan dalam struktur message menjadi 1.
fgets(message.mesg_text, 100, stdin): Program menggunakan fgets() untuk membaca perintah dari pengguna dan menyimpannya dalam mesg_text dalam struktur message.
msgsnd(msgid, &message, sizeof(message), 0): Program menggunakan msgsnd() untuk mengirim pesan yang ada dalam struktur message ke antrean pesan dengan ID msgid. 

Kode receiver.c :
```c
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
} message;

int main()
{
    key_t key;
    int msgid;
    int authenticated = 0;

    key = ftok(".", 18);

    msgid = msgget(key, 0666 | IPC_CREAT);

    while (1)
    {
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';
    }
    msgctl(msgid, IPC_RMID, NULL);
    return 0;
}
```
Penjelasan :
<stdio.h>: Header untuk fungsi input dan output standar.
<sys/ipc.h>: Header untuk bekerja dengan kunci IPC.
<sys/msg.h>: Header untuk berinteraksi dengan antrean pesan.
<string.h>: Header untuk operasi pemrosesan string.
<unistd.h>: Header yang berisi definisi sistem Unix.
<stdlib.h>: Header untuk alokasi memori dinamis dan fungsi-fungsi umum lainnya.
<sys/stat.h>: Header yang berisi definisi untuk status berkas.
mesg_type: Digunakan untuk menentukan jenis pesan yang akan diterima. Dalam kasus ini, pesan dengan jenis 1 akan diterima.
mesg_text: Digunakan untuk menyimpan teks pesan dengan panjang maksimum 100 karakter.
ftok() untuk menghasilkan kunci IPC berdasarkan lokasi file (titik, yang mengindikasikan direktori saat ini) dan angka 18.
msgget() untuk mengakses atau membuat antrean pesan dengan kunci yang sudah dihasilkan. Seperti sebelumnya, hak akses 0666 digunakan dan IPC_CREAT menunjukkan bahwa antrean pesan akan dibuat jika belum ada.

**B.** Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver.

Fungsi tambahan untuk decrypt:
```c
static const unsigned char d[] = {
    66,66,66,66,66,66,66,66,66,66,64,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,62,66,66,66,63,52,53,
    54,55,56,57,58,59,60,61,66,66,66,65,66,66,66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,66,66,66,66,66,66,26,27,28,
    29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,66,
    66,66,66,66,66,66
};

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen) {
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end) {
        unsigned char c = d[*in++];

        switch (c) {
        case WHITESPACE: continue;
        case INVALID:    return 1;
        case EQUALS:                 
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++; 
            if (iter == 4) {
                if ((len += 3) > *outLen) return 1; 
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3) {
        if ((len += 2) > *outLen) return 1; 
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2) {
        if (++len > *outLen) return 1; 
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len; 
    return 0;
}
```
Penjelasan :
Array d adalah tabel konstanta yang digunakan dalam proses dekoding Base64.
in: Pointer ke string yang akan di-decode (data Base64).
inLen: Ukuran (panjang) data yang akan di-decode.
out: Pointer ke buffer yang akan menampung hasil dekoding (data biner).
outLen: Pointer ke variabel yang akan menyimpan panjang data hasil dekoding.
end: Pointer yang menunjuk ke akhir string in.
iter: Digunakan untuk menghitung iterasi saat membaca karakter-karakter Base64.
buf: Digunakan untuk membangun hasil dekoding dalam bentuk sementara.
len: Digunakan untuk melacak panjang data yang telah di-decode.

Kode tambahan pada fungsi main() :
```c
if (strcmp(message.mesg_text, "CREDS") == 0)
{
    FILE *file = fopen("/home/nevarre/Documents/Christopher/users.txt", "r");

    if (file)
    {
        char line[1000];

        while (fgets(line, sizeof(line), file))
        {
            line[strcspn(line, "\n")] = '\0';

            char *username = strtok(line, ":");
            char *encoded_password = strtok(NULL, ":");

            if (username && encoded_password)
            {
                int string_length = strlen(encoded_password);

                char *decoded_password = base64decoder(encoded_password, string_length);
                printf("Username: %s, Password: %s\n", username, decoded_password);
            }
        }
    }
    fclose(file);
}
```
Penjelasan :
if memeriksa apakah pesan yang diterima (message.mesg_text) adalah "CREDS". Menggunakan fungsi strcmp(), yang membandingkan pesan dengan string "CREDS".
Jika pesan adalah "CREDS", maka kode akan mencoba membuka berkas /home/nevarre/Documents/Christopher/users.txt dalam mode baca ("r"). 
Program akan membaca berkas baris per baris menggunakan fgets(). 
Program akan memecah baris yang dibaca menjadi dua bagian, yaitu username dan encoded_password, dengan menggunakan fungsi strtok(). 
kode akan memeriksa apakah username dan encoded_password tidak NULL. Jika tidak, maka kode akan menghitung panjang string encoded_password dengan strlen(). Kemudian, kode akan memanggil base64decode() untuk mendekode password yang dienkripsi dalam format Base64. 
Terakhir, kode akan mencetak informasi pengguna, termasuk username dan password yang telah didekode, dengan menggunakan printf().

**C.** Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.

Tambahan pada fungsi main() :
```c
else if (strncmp(message.mesg_text, "AUTH: ", 6) == 0)
{
    char *auth_message = message.mesg_text + 6;


    char *auth_username = strtok(auth_message, " ");
    char *auth_password = strtok(NULL, "");

    if (auth_username && auth_password)
    {
        int status = 0;
        FILE *file = fopen("/home/nevarre/Documents/Christopher/users.txt", "r");

        if (file)
        {
            char line[1000];

            while (fgets(line, sizeof(line), file))
            {
                line[strcspn(line, "\n")] = '\0';

                char *username = strtok(line, ":");
                char *encoded_password = strtok(NULL, ":");

                if (username && encoded_password)
                {
                    int string_length = strlen(encoded_password);
                    char *decoded_password = base64decoder(encoded_password, string_length);

                    if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)
                    {
                        status = 1;
                        break;
                    }

                    free(decoded_password);
                }
            }
            fclose(file);
        }
        if (status)
        {
            printf("Authentication successful\n");
            authenticated = 1;
        }
        else
        {
            printf("Authentication failed\n");
            authenticated = 0;
            msgctl(msgid, IPC_RMID, NULL);
            return 0;
        }
    }
}
```
Penjelasan :
else if memeriksa apakah pesan yang diterima dimulai dengan "AUTH: ". Ini dilakukan dengan menggunakan fungsi strncmp(), yang memeriksa apakah enam karakter pertama dari pesan adalah "AUTH: ".
Jika pesan dimulai dengan "AUTH: ", program menghapus "AUTH: " dari pesan dengan cara menggeser pointer auth_message ke posisi setelah "AUTH: " dalam pesan message.mesg_text.
Selanjutnya, program memecah pesan menjadi dua bagian, yaitu auth_username dan auth_password, dengan menggunakan strtok().
Program memeriksa apakah auth_username dan auth_password tidak NULL.
Program kemudian membandingkan auth_username dan auth_password dengan informasi yang ditemukan dalam users.txt. Jika ada kecocokan, status diatur menjadi 1 (autentikasi berhasil) dan loop dihentikan.
Jika tidak ada kecocokan, kode melepaskan memori yang digunakan untuk decoded_password dan melanjutkan mencari dalam baris berikutnya.
Setelah loop selesai, program memeriksa nilai status. Jika autentikasi berhasil (status = 1), maka pesan "Authentication successful" dicetak ke layar, dan variabel authenticated diatur menjadi 1 untuk menunjukkan bahwa pengguna telah diotentikasi.
Jika autentikasi gagal (status = 0), pesan "Authentication failed" dicetak ke layar, dan variabel authenticated diatur menjadi 0.
Selain itu, program juga akan menghapus antrean pesan dengan memanggil msgctl() dengan argumen IPC_RMID dan kemudian menghentikan program dengan mengembalikan 0.

**D.** Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename .

Tambahan kode pada fungsi main() :
```c
else if (strncmp(message.mesg_text, "TRANSFER ", 9) == 0)
{
    if (authenticated == 1)
    {
        char *senderFile = message.mesg_text + 9;

        char source[300];
        char destination[300];

        sprintf(source, "Sender/%s", senderFile);
        sprintf(destination, "Receiver/%s", senderFile);

        struct stat st;
        if (stat(destination, &st) == 0)
        {
            printf("Found a duplicate file in 'Receiver' directory\n");
        }
    }
    else
    {
        printf("not auth");
    }
}
```
Penjelasan :
else if memeriksa apakah pesan yang diterima dimulai dengan "TRANSFER " (termasuk spasi setelah "TRANSFER"). Ini dilakukan menggunakan fungsi strncmp().
Program memeriksa nilai variable authenticated. Jika authenticated adalah 1, pengguna diperbolehkan melakukan transfer.
Program mengambil nama berkas yang akan ditransfer dari pesan yang diterima dengan variabel senderFile.
sprintf() untuk membuat nama berkas sumber dan tujuan. Berkas sumber adalah dalam direktori "Sender" dan berkas tujuan adalah dalam direktori "Receiver".
struct stat dan stat() untuk memeriksa apakah berkas dengan nama yang sama sudah ada dalam direktori "Receiver". Jika berkas dengan nama yang sama sudah ada, maka pesan "Found a duplicate file in 'Receiver' directory" dicetak ke layar.

**E.** Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.

Tambahkan pada kode bagian TRANSFER :
```c
else
{
    if (rename(source, destination) == 0)
    {
        if (stat(destination, &st) == 0)
        {
            printf("Ukuran file yang dipindah: %lld KB\n", (long long)st.st_size / 1024);
        }
        else
        {
            perror("Error mendapatkan ukuran file");
        }
    }
    else
    {
        perror("cant move file");
    }
}
```
Penjelasan :
Jika tidak ada berkas dengan nama yang sama dalam "Receiver", kode ini menggunakan rename() untuk mentransfer berkas dari direktori "Sender" ke "Receiver". Jika transfer berhasil (nilai yang dikembalikan oleh rename() adalah 0), kode ini memeriksa ukuran berkas yang telah ditransfer dan mencetaknya dalam kilobita (KB).

**F.** Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

Tambahan kode pada fungsi main() :
```c
else
{
    printf("UNKNOWN COMMAND\n");
    fflush(stdout);
}
```
Penjelasan :
Program akan menampilkan UNKNOWN COMMAND ketika perintah input yang lain.
fflush(stdout) digunakan untuk membersihkan buffer keluaran (output buffer) dari layar.

- Hasil program :
![Hasil Christopher](gambar/Christopher.png)

## Kendala
Kendala dalam mengenali perintah untuk receiver.  
Kendala dalam mengautentikasi username dan password.

# Soal 4 
## Deskripsi Program
Program ini adalah sebuah server socket sederhana yang dapat menerima koneksi dari client dan menampilkan pesan yang diterima. Berikut adalah langkah-langkah utama dari program ini:

# Langkah-langkah Pengerjaan
## Program Server Socket
1. Anda dapat mengonfigurasi port dan jumlah koneksi maksimum dengan mengubah nilai dari konstanta `PORT` dan `MAX_CONNECTIONS` di dalam kode program.
```c
#define PORT 8080
#define MAX_CONNECTIONS 5
```

2. Definisikan semua variabel yang akan digunakan dalam proses pembuatan, pengaturan, dan penerimaan koneksi pada server socket. Mereka menyimpan informasi-informasi penting yang diperlukan dalam operasi-operasi tersebut.
```c
int main() {
    int server_fd;
    int new_socket;
    int valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    int client_terhubung = 0;
    int pesan_terkirim = 0;
}
```

3. Membuat socket: Program membuat socket menggunakan fungsi `socket` dari library `sys/socket.h`. Jika pembuatan socket gagal, program akan menampilkan pesan error dan keluar.
```c
if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }
```

4. Mengatur opsi socket: Program menggunakan fungsi `setsockopt` untuk mengatur opsi socket. Opsi `SO_REUSEADDR` dan `SO_REUSEPORT` digunakan untuk memungkinkan penggunaan ulang alamat dan port.
```c
if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }
```

5. Binding address: Program menggunakan fungsi `bind` untuk mengikat alamat server ke socket. Jika binding gagal, program akan menampilkan pesan error dan keluar.
```c
if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }
```

6. Menunggu koneksi: Program menggunakan fungsi `listen` untuk memulai proses menunggu koneksi dari client. Maksimal 5 koneksi dapat terhubung secara bersamaan. Jika terpenuhi maka akan menampilkan pesan "**Server listening on port 8080**"
```c
if (listen(server_fd, MAX_CONNECTIONS) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d ..\n", PORT);
```

7. Menerima dan menampilkan pesan: Program masuk ke dalam loop tak terbatas untuk menerima koneksi dari client. Jika batas koneksi tercapai, program akan memberikan pesan bahwa tidak dapat menerima koneksi tambahan. Setiap pesan yang diterima dari client akan ditampilkan di konsol.
```c
while (1) {
        if (client_terhubung >= MAX_CONNECTIONS && !pesan_terkirim) {
            printf("Batas koneksi tercapai. Tidak dapat menerima client tambahan.\n");
            pesan_terkirim = 1;
        }
        if (client_terhubung < MAX_CONNECTIONS) {
            pesan_terkirim = 0;
        }
        
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        client_terhubung++; // Menambah jumlah client yg terhubung

        char buffer[1024] = {0};
        valread = read(new_socket, buffer, sizeof(buffer));
        printf("Pesan dari client: %s\n", buffer);
    }
```

### **Script full :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080
#define MAX_CONNECTIONS 5

int main() {
    int server_fd;
    int new_socket;
    int valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    int client_terhubung = 0;
    int pesan_terkirim = 0;

    // Membuat socket
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Mengatur opsi socket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Binding address
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Menunggu koneksi, maksimal terhubung 5 koneksi
    if (listen(server_fd, MAX_CONNECTIONS) < 0) {
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d ..\n", PORT);

    // Menerima dan menampilkan pesan
    while (1) {
        if (client_terhubung >= MAX_CONNECTIONS && !pesan_terkirim) {
            printf("Batas koneksi tercapai. Tidak dapat menerima client tambahan.\n");
            pesan_terkirim = 1;
        }
        if (client_terhubung < MAX_CONNECTIONS) {
            pesan_terkirim = 0;
        }
        
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("Accept failed");
            exit(EXIT_FAILURE);
        }

        client_terhubung++; // Menambah jumlah client yg terhubung

        char buffer[1024] = {0};
        valread = read(new_socket, buffer, sizeof(buffer));
        printf("Pesan dari client: %s\n", buffer);
    }

    return 0;
}

```

## Program Client Socket
### Deskripsi Program
Program ini adalah sebuah klien socket sederhana yang dapat terhubung ke server socket dan mengirim pesan ke server.

# Langkah-langkah Pengerjaan
Berikut adalah langkah-langkah utama dari program ini:
1. Mendefinisikan Port dan server IP yang digunakan
```c
#define PORT 8080
#define SERVER_IP "127.0.0.1"
```
2. Definisikan semua variabel yang akan digunakan dalam proses pembuatan, pengaturan, dan pengiriman pesan dari klien socket ke server socket. Mereka menyimpan informasi-informasi penting yang diperlukan dalam operasi-operasi tersebut.
```c
int main() {
    int sock = 0;
    struct sockaddr_in server_addr;
    char message[1024];
}
```

3. Membuat socket klien: Program membuat socket klien menggunakan fungsi `socket` dari library `sys/socket.h`. Jika pembuatan socket gagal, program akan menampilkan pesan error dan keluar.
```c
if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Gagal membuat socket \n");
        return -1;
    }
```

4. Mengonfigurasi alamat server: Program mengonfigurasi alamat server yang akan dihubungi. Alamat IP server dan port yang digunakan didefinisikan dalam konstanta `SERVER_IP` dan `PORT`.
```c
if(inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr) <= 0) {
        printf("\nInvalid address/Address not supported \n");
        return -1;
    }
```

5. Membuat koneksi ke server: Program menggunakan fungsi `connect` untuk membuat koneksi ke server socket. Jika koneksi gagal, program akan menampilkan pesan error dan keluar.
```c
if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        printf("\nGagal menghubungkan ke server \n");
        return -1;
    }

    printf("Koneksi ke server berhasil\n");
```

6. Mengirim pesan ke server: Setelah koneksi berhasil dibuat, program akan meminta pengguna untuk memasukkan pesan yang akan dikirim ke server. Pesan tersebut kemudian dikirim menggunakan fungsi `send`.
```c
printf("Masukkan pesan untuk dikirim ke server: ");
    fgets(message, sizeof(message), stdin);
    send(sock, message, strlen(message), 0);
    printf("Pesan terkirim\n");
```

### **Script full :**
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#define PORT 8080
#define SERVER_IP "127.0.0.1"

int main() {
    int sock = 0;
    struct sockaddr_in server_addr;
    char message[1024];

    // Membuat socket klien
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Gagal membuat socket \n");
        return -1;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    // Konversi alamat IPv4 dan mengatur server IP
    if(inet_pton(AF_INET, SERVER_IP, &server_addr.sin_addr) <= 0) {
        printf("\nInvalid address/Address not supported \n");
        return -1;
    }

    // Membuat koneksi ke server
    if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        printf("\nGagal menghubungkan ke server \n");
        return -1;
    }

    printf("Koneksi ke server berhasil\n");

    // Kirim pesan ke server
    printf("Masukkan pesan untuk dikirim ke server: ");
    fgets(message, sizeof(message), stdin);
    send(sock, message, strlen(message), 0);
    printf("Pesan terkirim\n");

    return 0;
}

```

### Cara Menjalankan Program

1. Kompilasi kedua program tersebut menggunakan perintah berikut di terminal:

```bash
gcc server.c -o server
```

```bash
gcc client.c -o clien
```

lalu periksa apakah sudah terbuat file kompilasinya.

![kompilasi2](gambar/kompilasi2.png)

2. Jalankan kedua program dengan perintah berikut: 
```bash
./server
```
kemudian akan muncul pesan Port terhubung

![run server](gambar/run-server.png)

```bash
./client
```
lalu client akan disuruh memasukkan input berupa pesan yang akan dikirim ke server seperti berikut

![run client](gambar/run-client.png)

3. Pesan dari client tersebut akan terkirim di server, seperti berikut

![pesan terkirim](gambar/pesan-terkirim.png)

4. Jika server sudah terkoneksi dengan 5 client, maka akan mengeluarkan output seperti berikut

![limit](gambar/limit.png)
